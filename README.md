### Marat Biriushev

## Application
The flask_app directory contains an application that returns "Hello world! Time is: TIME_IN_MOSCOW" when accessing it. Dockerfile, with main image: python: 3. 1. layer - the flask user is created, 2. layer - requirements.txt is transferred to the container, 3. layer - dependencies are installed, 4. layer - the code is transferred to the container, the root is changed to the "flask" user, 5. layer - the application starts.

## Infrastructure
The terraform directory describes the infrastructure for deploying VMs on Google Cloud. The provider is "google". The variables "project", "credentials_file" are stored in the terraform.tfvars file. The "region", "zone" variables are stored in the variables.tf file.

Plugin "random_id" is used for convenient name assignment for VM. The image is centos-8.
The public SSH key is passed to connect to the VM.
Added firewall rules allowing connections on tcp ports 80, 443 and 5000.
Added the output "ip" variable to the outputs.tf file to get the external IP of the VM.
## Bootstrapping
The ansible directory contains a playbook.yml that uses 2 roles:
Docker role
Installs the latest version of docker-ce on the VM.

Site role
Ports the application code and Dockerfile to the target VM.
Builds the app image and launches the app container.