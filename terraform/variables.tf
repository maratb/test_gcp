variable "project" { }

variable "credentials_file" { }

variable "region" {
  default = "europe-central2"
}

variable "zone" {
  default = "europe-central2-a"
}

