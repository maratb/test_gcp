provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project
  region      = var.region
  zone        = var.zone

}

// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
  byte_length = 8
}

// A single Compute Engine instance
resource "google_compute_instance" "default" {
  name         = "test-vm-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-8"
  }
}

  network_interface {
    network = "default"

    access_config {
    // Include this section to give the VM an external ip address
  }
}
metadata = {
  ssh-keys = "marat:${file("~/.ssh/id_rsa.pub")}"
  }
  labels = {
    key = "test"
  }
}
resource "google_compute_firewall" "default" {
  name    = "flask-app-firewall"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "5000"]
  }
}
